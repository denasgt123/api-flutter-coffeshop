<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('products')->insert([
			'name' => 'Original Coffee',
			'description' => 'A original coffee from local farm',
			'image' => 'img/coffee.jpg',
			'price' => 25000,
		]);
	}
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

	/**
	 * Get all of the transaction_detail for the Transaction
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function transaction_detail()
	{
		return $this->hasMany(TransactionDetail::class);
	}
}
